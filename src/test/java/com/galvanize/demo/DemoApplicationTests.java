package com.galvanize.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static com.galvanize.demo.DemoApplication.returnNumber;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}
	@Test
	void returnNumberShouldReturnOne(){
		assertEquals(1, returnNumber());
	}

}
